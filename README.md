# dmenu - dynamic menu
dmenu is an efficient dynamic menu for X.

## Requirements
In order to build dmenu you need the Xlib header files.

## Installation
Clone the Repository:
```bash
git clone https://gitlab.com/crimsonmyth/dmenu.git 
```

## Running dmenu
-------------
Use The Following command to run dmenu:
```bash
dmenu_run -l 10 -g 7 -bw 2
```
`-l 10` Specifies the lines, and `-g 7` Specifies columns.   

I would recommend binding the above command to a keybinding using sxhkd or bind it through your window manager. 
